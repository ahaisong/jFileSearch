package net.qqxh.resolve2text.exception;

public class File2TextResolveNotFondException extends Exception{
    public File2TextResolveNotFondException(String message) {
        super(message);
    }
}
