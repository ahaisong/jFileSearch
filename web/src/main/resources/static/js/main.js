$(function () {
    page.base("");
    page("syshelp", function () {
        singleFileResolveApp.getInstance().init();
    });
    page("fileSearchApp", function () {
        new fileSearchApp().init();
    });
   /* page("fileManagerApp", function () {
        new fileManagerApp().init();
    });*/
    page("fileCleanApp", function () {
        singleFileCleanApp.getInstance().init();
    });
    page({hashbang: true});
    page.start();
    loadMyDesktop();
});

function loadMyDesktop() {
    $.ajax({
        url: "/getMyDesktop",
        type: 'get',
        contentType: "application/x-www-form-urlencoded",
        type: "get",
        cache: false,
        success: function (msg) {

            if (msg.data) {
                $(".desktop").empty();
                $(msg.data).each(function (index, file) {
                    var filename = file.name;
                    var filearr;
                    if (filename != null) {
                        filearr = filename.split(".");
                    } else {
                        filearr = "file.file".split(".");
                    }
                    var name = filearr[0];
                    var fix = filearr[1];

                    var shortcut = $("<div class='shortcut'/>").append(
                        $("<div class='icon'/>").addClass(fix)
                    ).append(
                        $("<div class='title'/>").html(name)
                    ).append(
                        $("<span class='fa fa-times remove' aria-hidden='true'></span>").click(
                            function () {
                                $.ajax({
                                    url: "/removeFromDesktop",
                                    type: 'delete',
                                    contentType: "application/x-www-form-urlencoded",
                                    type: "get",
                                    data: {
                                        path: file.id
                                    },
                                    cache: false,
                                    success: function (msg) {
                                        loadMyDesktop();
                                        resResolve(msg);
                                    },
                                    error: function (msg) {
                                        loadMyDesktop();
                                        resResolve(msg);
                                    }
                                });
                            }
                        )
                    ).attr("title", file.id).dblclick(function () {
                        window.open('/showbypath?path=' + encodeURIComponent(file.id), genNonDuplicateID(3));
                    });
                    $(".desktop").append(shortcut);
                })
            }
        },
        error: function (res) {

        }
    });
}

function clearDesktop() {
    $.ajax({
        url: "/clearDesktop",
        type: 'delete',
        contentType: "application/x-www-form-urlencoded",

        cache: false,
        success: function (msg) {
            loadMyDesktop();
            resResolve(msg);
        },
        error: function (msg) {
            loadMyDesktop();
            resResolve(msg);
        }
    });
}
function genNonDuplicateID (randomLength) {
    return Number(Math.random().toString().substr(3, randomLength) + new Date().getTime()).toString(36)
}
window.onload = function () {
    var myClock = new Clock();
    $("#systemTime").html(myClock.toString())
}

function startFileSolveApp() {
    page("syshelp");
}

function startFileSearchApp() {
    page("fileSearchApp");

}

function startFileManagerApp() {
    page("fileManagerApp");

}

function startFileCleanApp() {
    page("fileCleanApp");
}


function resResolve(res) {
    if (res.status == "200") {
        $.Toast(res.msg, res.detail, "success", {
            has_icon: true,
            has_close_btn: true,
            fullscreen: false,
            timeout: 3000,
            sticky: false,
            has_progress: true,
            rtl: false,
        });
    } else {
        $.Toast(res.msg, res.detail, "error", {
            has_icon: true,
            has_close_btn: true,
            fullscreen: false,
            timeout: 3000,
            sticky: false,
            has_progress: true,
            rtl: false,
        });
    }
}

function copyToClipboard(text) {
    if (text.indexOf('-') !== -1) {
        let arr = text.split('-');
        text = arr[0] + arr[1];
    }
    var textArea = document.createElement("textarea");
    textArea.style.position = 'fixed';
    textArea.style.top = '0';
    textArea.style.left = '0';
    textArea.style.width = '2em';
    textArea.style.height = '2em';
    textArea.style.padding = '0';
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? '成功复制到剪贴板' : '该浏览器不支持点击复制到剪贴板';
        $.Toast("成功", msg, "success", {
            has_icon: true,
            has_close_btn: true,
            fullscreen: false,
            timeout: 1000,
            sticky: false,
            has_progress: true,
            rtl: false,
        });
    } catch (err) {
        $.Toast("失败", '该浏览器不支持点击复制到剪贴板', "error", {
            has_icon: true,
            has_close_btn: true,
            fullscreen: false,
            timeout: 1000,
            sticky: false,
            has_progress: true,
            rtl: false,
        });
    }
    document.body.removeChild(textArea);
}

function changeTheme(theme) {
    $.ajax({
        url: "/changeTheme",
        type: 'post',
        data: {theme: theme},
        contentType: "application/x-www-form-urlencoded",
        cache: false,
        success: function (msg) {
            $("body").attr("class", theme);
            resResolve(msg);
        },
        error: function (msg) {
            resResolve(msg);
        }
    });

}