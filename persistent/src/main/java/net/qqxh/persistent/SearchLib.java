package net.qqxh.persistent;

import java.io.Serializable;

public class SearchLib implements Serializable {
    private static final long serialVersionUID = -7510469744048281760L;
    private String libId;
    private String fileSourceDir;
    private String fileViewDir;
    private String svnRepository_url;
    private String svnUsername;
    private String svnPassword;
    private String esIndex;
    private String remoteRepositoryMode;
    private String analysisMode;
    public String getLibId() {
        return libId;
    }

    public void setLibId(String libId) {
        this.libId = libId;
    }

    public String getFileSourceDir() {
        return fileSourceDir;
    }

    public void setFileSourceDir(String fileSourceDir) {
        this.fileSourceDir = fileSourceDir;
    }

    public String getFileViewDir() {
        return fileViewDir;
    }

    public void setFileViewDir(String fileViewDir) {
        this.fileViewDir = fileViewDir;
    }

    public String getSvnRepository_url() {
        return svnRepository_url;
    }

    public void setSvnRepository_url(String svnRepository_url) {
        this.svnRepository_url = svnRepository_url;
    }

    public String getSvnUsername() {
        return svnUsername;
    }

    public void setSvnUsername(String svnUsername) {
        this.svnUsername = svnUsername;
    }

    public String getSvnPassword() {
        return svnPassword;
    }

    public void setSvnPassword(String svnPassword) {
        this.svnPassword = svnPassword;
    }

    public String getEsIndex() {
        return esIndex;
    }

    public void setEsIndex(String esIndex) {
        this.esIndex = esIndex;
    }

    public String getRemoteRepositoryMode() {
        return remoteRepositoryMode;
    }

    public void setRemoteRepositoryMode(String remoteRepositoryMode) {
        this.remoteRepositoryMode = remoteRepositoryMode;
    }

    public String getAnalysisMode() {
        return analysisMode;
    }

    public void setAnalysisMode(String analysisMode) {
        this.analysisMode = analysisMode;
    }
}
