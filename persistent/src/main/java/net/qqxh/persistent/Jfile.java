package net.qqxh.persistent;

import java.io.File;


public class Jfile {
    private String rid;
    private String text;
    private String name;
    private String path;
    private String filePhase;
    private String viewPath;
    private String viewName;
    private String fix;
    private String viewFix;
    public Jfile(File file) {
        this.name = file.getName();
        this.path = file.getPath();
    }
    public Jfile() {
    }
    public String getFix() {
        String suffix = name.substring(name.lastIndexOf(".") + 1);
        return suffix;
    }
    public void setFix(String fix) {
        this.fix = fix;
    }

    public String getViewFix() {
        return viewFix;
    }

    public void setViewFix(String viewFix) {
        this.viewFix = viewFix;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getViewName() {
        File f = new File(this.viewPath);
        return f.getName();
    }

    public String getViewPath() {
        return viewPath;
    }

    public void setViewPath(String viewPath) {
        this.viewPath = viewPath;
    }


    public String getFilePhase() {
        return filePhase;
    }

    public void setFilePhase(String filePhase) {
        this.filePhase = filePhase;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
}
